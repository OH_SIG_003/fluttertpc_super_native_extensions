import 'package:super_clipboard/src/util.dart';
import 'package:super_native_extensions/raw_clipboard.dart' as raw;
import 'package:flutter/services.dart';

import 'writer.dart';

final _methodChannel = const OptionalMethodChannel('super_native_extensions');

extension ClipboardWriterItemDataProvider on DataWriterItem {
  
  Future<raw.DataProvider> asDataProvider() async {
    final representations = <raw.DataRepresentation>[];
    for (final data in this.data) {
      for (final representation in (await data).representations) {
        if (representation is raw.DataRepresentationSimple) {
          representations.add(raw.DataRepresentation.simple(
              format: representation.format,
              data: await getData(representation: representation),
            ));
        } else {
          representations.add(representation);
        }
      }
    }
    return raw.DataProvider(
      representations: representations,
      suggestedName: suggestedName,
    );
  }

  Object? getData({representation: raw.DataRepresentationSimple}) async {
    if (_methodChannel == null) {
      return representation.data;
    }
    final preprocessedData = await _methodChannel.invokeMethod<Object>('dataPreprocess', <String, dynamic>{
        'data': representation.data, 
        'format': representation.format,
      });
    return preprocessedData != null ? preprocessedData : representation.data;
  }

  Future<raw.DataProviderHandle> registerWithDataProvider(
      raw.DataProvider provider) async {
    final handle = await provider.register();
    final onDisposed = this.onDisposed as SimpleNotifier;
    final onRegistered = this.onRegistered as SimpleNotifier;
    handle.onDispose.addListener(() {
      onDisposed.notify();
      onDisposed.dispose();
      onRegistered.dispose();
    });
    onRegistered.notify();
    return handle;
  }
}
