/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use std::{
    fmt::Debug,
    fs,
    io::Write,
    path::{Path, PathBuf},
    process::Command,
};

use anyhow::{Context, Result};
use log::{debug, info};
use semver::Version;

use super::utils::*;

#[derive(Debug)]
enum Target {
    Arm64,
}

impl Target {
    fn rust_target(&self) -> &'static str {
        match self {
            Target::Arm64 => "aarch64-unknown-linux-ohos",
        }
    }

    fn target_dir(&self) -> &'static str {
        match self {
            Target::Arm64 => "arm64-v8a",
        }
    }

    fn from_flutter_target(s: &str) -> Option<Target> {
        match s {
            "ohos" => Some(Target::Arm64),
            _ => None,
        }
    }
}

fn get_targets() -> Vec<Target> {
    let platforms = std::env::var("CARGOKIT_TARGET_PLATFORMS")
        .ok()
        .unwrap_or_else(|| "".into());
    platforms
        .split(',')
        .into_iter()
        .filter_map(Target::from_flutter_target)
        .collect()
}

fn is_release() -> bool {
    let configuration = std::env::var("CARGOKIT_BUILD_MODE")
        .ok()
        .unwrap_or_else(|| "release".into());
    configuration != "debug"
}

#[cfg(target_os = "macos")]
const ARCH: &str = "darwin-x86_64";
#[cfg(target_os = "linux")]
const ARCH: &str = "linux-x86_64";
#[cfg(target_os = "windows")]
const ARCH: &str = "windows-x86_64";

#[cfg(target_os = "windows")]
const CLANG_TOOL_EXTENSION: &str = ".cmd";

#[cfg(not(target_os = "windows"))]
const CLANG_TOOL_EXTENSION: &str = "";

// Workaround for libgcc missing in NDK23, inspired by cargo-ndk
fn libgcc_workaround(build_dir: &Path, ndk_version: &Version) -> Result<String> {
    let workaround_dir = build_dir
        .join("cargokit")
        .join("libgcc_workaround")
        .join(ndk_version.major.to_string());
    fs::create_dir_all(&workaround_dir)?;
    if ndk_version.major >= 23 {
        let mut file = std::fs::File::create(workaround_dir.join("libgcc.a"))?;
        file.write_all(b"INPUT(-lunwind)")?;
    } else {
        // Other way around, untested, forward libgcc.a from libunwind once Rust
        // gets updated for NDK23+.
        let mut file = std::fs::File::create(workaround_dir.join("libunwind.a"))?;
        file.write_all(b"INPUT(-lgcc)")?;
    }

    let mut rustflags = match std::env::var("CARGO_ENCODED_RUSTFLAGS") {
        Ok(val) => val,
        Err(std::env::VarError::NotPresent) => "".to_string(),
        Err(std::env::VarError::NotUnicode(_)) => {
            log::error!("RUSTFLAGS environment variable contains non-unicode characters");
            std::process::exit(1);
        }
    };

    if !rustflags.is_empty() {
        rustflags.push('\x1f');
    }
    rustflags.push_str("-L\x1f");
    rustflags.push_str(&workaround_dir.to_string_lossy());

    Ok(rustflags)
}

fn pick_existing(paths: Vec<PathBuf>) -> Option<PathBuf> {
    paths.into_iter().find(|p| p.exists())
}

fn build_for_target(target: &Target) -> Result<()> {

    let build_dir = path_from_env("CARGOKIT_BUILD_DIR")?;
    let output_dir = path_from_env("CARGOKIT_OUTPUT_DIR")?;
    let lib_name = string_from_env("CARGOKIT_LIB_NAME")?;

    let mut cmd = Command::new("cargo");
    cmd.arg("build");
    cmd.arg("--manifest-path");
    cmd.arg(path_from_env("CARGOKIT_MANIFEST_DIR")?.join("Cargo.toml"));
    cmd.arg("-p");
    cmd.arg(&lib_name);
    if is_release() {
        cmd.arg("--release");
    }
    cmd.arg("--target");
    cmd.arg(target.rust_target());
    cmd.arg("--target-dir");
    cmd.arg(&build_dir);

    run_command(cmd)?;

    let output_dir = output_dir.join(target.target_dir());
    fs::create_dir_all(&output_dir)?;

    let lib_name_full = format!("lib{}.so", lib_name);
    let src = build_dir
        .join(target.rust_target())
        .join(if is_release() { "release" } else { "debug" })
        .join(&lib_name_full);
    let dst = output_dir.join(&lib_name_full);
    fs::copy(&src, &dst)
        .with_context(|| format!("dst: {:?}", dst))
        .with_context(|| format!("src: {:?}", src))?;

    Ok(())
}

pub fn build_ohos() -> Result<()> {
    let targets = get_targets();
    debug!("Building for targets: {:?}", targets);
    println!("Building for targets: {:?}", targets);

    let installed_targets = installed_targets()?;
    for target in &targets {
        if !installed_targets.contains(&target.rust_target().to_owned()) {
            info!("Installing target {}...", target.rust_target());
            install_target(target.rust_target())?;
        }
    }

    for target in &targets {
        debug!("Building for {}...", target.rust_target());
        build_for_target(target)?;
    }

    Ok(())
}
